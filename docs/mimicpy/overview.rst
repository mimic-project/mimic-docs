Overview
========

MiMiCPy is a freely distributed component of the MiMiC package, consisting of tools to easily prepare and debug MiMiC input files. The instructions for installation are described in the :ref:`mimicpy`.

MiMiCPy primarily consists of a set of command line tools to easily prepare and debug complex CPMD and GROMACS input files for a MiMiC run. The list of tools are:

:PrepQM: Prepare the QM and MM input files from MM data
:CPMD2Coords: Convert the atom coordinates in a CPMD/MiMiC input file to a GRO or PDB file
:FixTop: Fix the :code:`[ atomtypes ]` section of the GROMACS MM topology for a MiMiC run
:CPMDid: Find the CPMD indices of QM and MM atoms in the system
:Geom2Coords: Convert the atom coordinates in a CPMD GEOMETRY file to a GRO or PDB file

Each tool is described in following sections. In addition to the command line tools, plugins of the PrepQM tool for VMD and PyMOL have also been provided. MiMiCPy can also be used as a Python library for more advanced functionalities.

For more details about MiMiCPy, we refer to the following article:

B. Raghavan, F. K. Schackert, A. Levy, S. K. Johnson, E. Ippoliti, D. Mandelli, J. M. H. Olsen, U. Rothlisberger, and P. Carloni,
*MiMiCPy: An Efficient Toolkit for MiMiC-Based QM/MM Simulations*,
J. Chem. Inf. Model. **63**, 1406-1412 (2023).
DOI: `10.1021/acs.jcim.2c01620 <https://doi.org/10.1021/acs.jcim.2c01620>`_
