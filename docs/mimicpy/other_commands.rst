Other Commands
==============

CPMD2Coords
-----------

:code:`mimicpy cpmd2coords` is a tool to convert the atom coordinates in a MiMiC CPMD input file to a GRO or a PDB file. The coordinates in the CPMD input script can be hard to read and debug. This tool uses the information for the :code:`&MIMIC` and :code:`&ATOMS` sections, and combines it with the MM topology to create a coordinate file that can be easily read and visualized by a molecular visualization software. This can assist in debugging the QM region in a MiMiC run, and confirm the correct atoms were placed in the QM region.

Options
^^^^^^^

:code:`mimicpy cpmd2coords` has the following options:

Input Options
~~~~~~~~~~~~~
  -top [.top]
	The file name of the MM topology file. Currently, only GROMACS topology files are accepted.

  -inp [.inp]
	The file name of the CPMD input script with the atoms coordinates formatted for a MiMiC run.

Output Options
~~~~~~~~~~~~~~
  -coords [.gro/.pdb] (mimic.gro)
	The file name of the output coordinate file. Currently, GRO and PDB formats are supported. By default, it is :code:`mimic.gro`.

Other Options
~~~~~~~~~~~~~
  -guess (True)
	Toggling guessing of atomic species in the MM topology. For more details refer to the :ref:`PrepQM section<prepqm other options>`.

  -nsa [.txt/.dat]
  	File name of file containing list of non-standard atom types in a two columns format. For more details refer to the :ref:`PrepQM section<prepqm other options>`.

  -ff
  	Pass the directory containing the force field folder with this flag. For more details refer to the :ref:`PrepQM section<prepqm other options>`.

  -buf (1000)
	Buffer size for reading input topology. Should only be used if unexpected errors occur in reading of topology.

FixTop
------

:code:`mimicpy fixtop` is a tool to write a consolidated :code:`[ atomtypes ]` section in the GROMACS topology, containing the atomic elements of all (QM and MM) atoms in the system. It is important for CPMD to have access to the atomic element information all atoms. If some of them are missing, it can lead to garbage values and segmentation faults in a MiMiC run. This is especially true if an MM atom, without atomic elements information, participates in a QM-MM bond.

Often times, non-standard atoms in a GROMACS topology may not have atom species information specified. :code:`FixTop` adds an :code:`[ atomtypes ]` section in a GROMACS ITP file. These elements are either guessed by MiMiCPy, or can be explicitly specified with the :code:`-nsa` flag.

Options
^^^^^^^

:code:`mimicpy fixtop` has the following options:

Input Options
~~~~~~~~~~~~~
  -top [.top]
	The file name of the MM topology file. Currently, only GROMACS topology files are accepted.

Output Options
~~~~~~~~~~~~~~
  -fix [.itp] (atomtypes.itp)
	The file name of the GROMACS ITP file where the :code:`[ atomtypes ]` section, containing the atomic elements of all atoms, will be written to.

Other Options
~~~~~~~~~~~~~
  -guess (True)
	Toggling guessing of atomic elements in the MM topology. For more details refer to the :ref:`PrepQM section<prepqm other options>`.

  -nsa [.txt/.dat]
  	File name of file containing list of non-standard atom types in a two columns format. For more details refer to the :ref:`PrepQM section<prepqm other options>`.

  -ff
  	Pass the directory containing the force field folder with this flag. For more details refer to the :ref:`PrepQM section<prepqm other options>`.

  -cls [.itp .itp .itp ..]
  	Pass a space separated list of GROMACS ITP files, where the :code:`[ atomtypes ]` section will be cleared from all of them. GROMACS allows only one :code:`[ atomtypes ]` section in a topology. The main :code:`[ atomtypes ]` section will be written to the file specified in the :code:`-fix` flag. All other :code:`[ atomtypes ]` sections need to be cleared.

  -buf (1000)
	Buffer size for reading input topology. Should only be used if unexpected errors occur in reading of topology.

CPMDid
------

:code:`mimicpy cpmdid` is a tool to print out the CPMD IDs of both QM and MM atoms. These IDs are needed to select atoms in the CPMD script when performing certain commands like applying constraints, adding multiple thermostats, etc. The MM IDs of atoms from GROMACS are reshuffled by CPMD in a non-obvious way. This tool keeps track of the conversion of MM IDs to QM IDs. The atoms can be selected by the MiMiCPy selection language (described :ref:`here<mimicpy sele lang>`).

Options
^^^^^^^

:code:`mimicpy cpmdid` has the following options:

Input Options
~~~~~~~~~~~~~
  -top [.top]
	The file name of the MM topology file. Currently, only GROMACS topology files are accepted.

  -inp [.inp]
	The file name of the CPMD input script with all information for a MiMiC run.

Output Options
~~~~~~~~~~~~~~
  -out [.txt]
	An optional text file to write the results to.

Other Options
~~~~~~~~~~~~~
  -sele [.txt/.dat]
	The file name of the list of selection commands in text format. The selection commands to select the QM region atoms can be entered in the interactive 	session, or optionally passed as a file. The format of the selection keywords is given in the :ref:`following section<mimicpy sele lang>`.

  -ff
  	Pass the directory containing the force field folder with this flag. For more details refer to the :ref:`PrepQM section<prepqm other options>`.

  -buf (1000)
	Buffer size for reading input topology. Should only be used if unexpected errors occur in reading of topology.


Geom2Coords
-----------

:code:`mimicpy geom2coords` is a tool to convert the a CPMD GEOMETRY or GEOMETRY.xyz file to a GRO or PDB file for easy visualization. Although, the QM and MM trajectories contain the same information (especially when the MiMiC simulation outputs physically correct results), there are often times when they do not match. In these events, visualizing the CPMD GEOMETRY output can be helpful in diagnosing errors with the simulation. This tool provides an easy to use interface for this purpose.

Options
^^^^^^^

:code:`mimicpy geom2coords` has the following options:

Input Options
~~~~~~~~~~~~~
  -geom [GEOMETRY/GEOMETRY.xyz]
	The file name of the CPMD GEOMETRY file. Can be a plain GEOMETRY file, or an CPMD XYZ file.

  -top [.top]
	The file name of the MM topology file. Currently, only GROMACS topology files are accepted.

  -inp [.inp]
	The file name of the CPMD input script with all information for a MiMiC run.

Output Options
~~~~~~~~~~~~~~
  -coords [.gro/.pdb] (GEOMERY.gro)
	The file name of the output coordinates. Currently, only GRO and PDB files are supported.

Other Options
~~~~~~~~~~~~~
  -ff
  	Pass the directory containing the force field folder with this flag. For more details refer to the :ref:`PrepQM section<prepqm other options>`.

  -buf (1000)
	Buffer size for reading input topology. Should only be used if unexpected errors occur in reading of topology.
