About
=====

MiMiC is a framework for the development of multiscale models and for running
multiscale simulations related to computational chemistry. :cite:p:`mimic-1,mimic-2`
The goal is to enable flexible and efficient implementations of multiscale
models with support for multiple subsystems that each can be described at
different resolutions and levels of theory.

Spatial resolutions may range from a quantum mechanical description of
electrons, over atomistic molecular mechanics and coarse-grained modeling, to
continuum models. However, MiMiC itself does not calculate any subsystem
contributions (e.g., energy and forces) on its own except potentially for
contributions arising from the interactions between subsystems. Instead, MiMiC
relies on external programs using a multiple-program multiple-data (MPMD) model
with loose coupling between programs. This allows high flexibility while at the
same time retaining a high degree of computational efficiency by letting the
external programs run concurrently and by exploiting the fact that different
programs have been optimized for their own specific use case.

Specifically, MiMiC couples a main driver (which runs a molecular dynamics
simulation or performs some other type of calculation) to a set of external
programs, each of which concurrently computes the contributions that are
relevant to a specific subsystem using their own optimal parallelization
strategy. Crucial for this strategy is the efficient communication between
programs which is achieved through the use of a lightweight communication
library (MiMiC communication library aka MCL), which was developed specifically
for this purpose. MCL comes with a simple C and Fortran API that avoids major
intervention in the source code of the external programs. The underlying data
transfer uses MPI, which gives immediate access to high-speed interconnects
such as InfiniBand, but it can be seamlessly expanded to other communication
mechanisms without altering the API.

For more details about the MiMiC framework, and the theory and performance of
a MiMiC-based QM/MM implementation, we refer to the following two articles:

1. J. M. H. Olsen, V. Bolnykh, S. Meloni, E. Ippoliti, M. P. Bircher, P. Carloni, U. Rothlisberger,
   *MiMiC: A Novel Framework for Multiscale Modeling in Computational Chemistry*,
   J. Chem. Theory Comput. **15**, 3810–3823 (2019).
   DOI: `10.1021/acs.jctc.9b00093 <https://doi.org/10.1021/acs.jctc.9b00093>`_
2. V. Bolnykh, J. M. H. Olsen, S. Meloni, M. P. Bircher, E. Ippoliti, P. Carloni, U. Rothlisberger,
   *Extreme Scalability of DFT-Based QM/MM MD Simulations Using MiMiC*,
   J. Chem. Theory Comput. **15**, 5601–5613 (2019).
   DOI: `10.1021/acs.jctc.9b00424 <https://doi.org/10.1021/acs.jctc.9b00424>`_
