Citation
========

Please cite these papers if you used MiMiC in your work:

* J. M. H. Olsen, V. Bolnykh, S. Meloni, E. Ippoliti, M. P. Bircher, P. Carloni, U. Rothlisberger,
  *MiMiC: A Novel Framework for Multiscale Modeling in Computational Chemistry*,
  J. Chem. Theory Comput. **15**, 3810-3823 (2019).
  DOI: `10.1021/acs.jctc.9b00093 <https://doi.org/10.1021/acs.jctc.9b00093>`_
* V. Bolnykh, J. M. H. Olsen, S. Meloni, M. P. Bircher, E. Ippoliti, P. Carloni, U. Rothlisberger,
  *Extreme Scalability of DFT-Based QM/MM MD Simulations Using MiMiC*,
  J. Chem. Theory Comput. **15**, 5601-5613 (2019).
  DOI: `10.1021/acs.jctc.9b00424 <https://doi.org/10.1021/acs.jctc.9b00424>`_

And this paper if you used MiMiCPy:

* B. Raghavan, F. K. Schackert, A. Levy, S. K. Johnson, E. Ippoliti, D. Mandelli, J. M. H. Olsen, U. Rothlisberger, P. Carloni,
  *MiMiCPy: An Efficient Toolkit for MiMiC-Based QM/MM Simulations*,
  J. Chem. Inf. Model. **63**, 1406-1412 (2023).
  DOI: `10.1021/acs.jcim.2c01620 <https://doi.org/10.1021/acs.jcim.2c01620>`_

In addition, in the interest of reproducibility, we ask that you also cite the following software
(remembering to update the version number and DOI according to the version that was used):

* J. M. H. Olsen, V. Bolnykh, S. Meloni, E. Ippoliti, P. Carloni, and U. Rothlisberger,
  *MiMiC: A Framework for Multiscale Modeling in Computational Chemistry (v0.2.0)*,
  GitLab, **2022**.
  DOI: `10.5281/zenodo.7304688 <https://doi.org/10.5281/zenodo.7304688>`_.
  See https://mimic-project.org/.
* V. Bolnykh, J. M. H. Olsen, S. Meloni, E. Ippoliti, P. Carloni, and U. Rothlisberger,
  *MiMiC Communication Library (v2.0.2)*,
  GitLab, **2022**.
  DOI: `10.5281/zenodo.7497400 <https://doi.org/10.5281/zenodo.7497400>`_.
  See https://mimic-project.org/.
* B. Raghavan, and F. K. Schackert,
  *MiMiCPy (v0.2.1)*,
  GitLab, **2023**.
  DOI: `10.5281/zenodo.7746282 <https://doi.org/10.5281/zenodo.7746282>`_.
  See https://mimic-project.org/.

For your convenience here are BibTeX entries that you can use

.. code-block:: bibtex

    @article{mimic-1,
      title = {{MiMiC: A Novel Framework for Multiscale Modeling in Computational Chemistry}},
      author = {Olsen, J{\'o}gvan Magnus Haugaard and Bolnykh, Viacheslav and
                Meloni, Simone and Ippoliti, Emiliano and Bircher, Martin P. and
                Carloni, Paolo and Rothlisberger, Ursula},
      journal = {J. Chem. Theory Comput.},
      volume = {15},
      number = {6},
      pages = {3810-3823},
      month = {jun},
      year = {2019},
      url = {https://doi.org/10.1021/acs.jctc.9b00093},
      doi = {10.1021/acs.jctc.9b00093}
    }

    @article{mimic-2,
      title = {{Extreme Scalability of DFT-Based QM/MM MD Simulations Using MiMiC}},
      author = {Bolnykh, Viacheslav and Olsen, J{\'o}gvan Magnus Haugaard and
                Meloni, Simone and Bircher, Martin P. and Ippoliti, Emiliano and
                Carloni, Paolo and Rothlisberger, Ursula},
      journal = {J. Chem. Theory Comput.},
      volume = {15},
      number = {10},
      pages = {5601-5613},
      month = {oct},
      year = {2019},
      url = {https://doi.org/10.1021/acs.jctc.9b00424},
      doi = {10.1021/acs.jctc.9b00424}
    }

    @article{mimicpy,
      title = {{MiMiCPy: An Efficient Toolkit for MiMiC-Based QM/MM Simulations}},
      author = {Raghavan, Bharath and Schackert, Florian K. and Levy, Andrea and
                Johnson, Sophia K. and Ippoliti, Emiliano and Mandelli, Davide and
                Olsen, J{\'o}gvan Magnus Haugaard and Rothlisberger, Ursula and
                Carloni, Paolo},
      journal = {J. Chem. Inf. Model.},
      volume = {63},
      number = {5},
      pages = {1406-1412},
      month = {mar},
      year = {2023},
      url = {https://doi.org/10.1021/acs.jcim.2c01620},
      doi = {10.1021/acs.jcim.2c01620}
    }

    @misc{mimic:0.2.0,
      author = {Olsen, J{\'o}gvan Magnus Haugaard and Bolnykh, Viacheslav and
                Meloni, Simone and Ippoliti, Emiliano and Carloni, Paolo and
                Rothlisberger, Ursula},
      title = {{MiMiC: A Framework for Multiscale Modeling in Computational Chemistry (v0.2.0)}},
      publisher = {GitLab},
      year = {2022},
      doi = {10.5281/zenodo.7304688},
      note = {See https://mimic-project.org/}
    }

    @misc{mcl:2.0.2,
      author = {Bolnykh, Viacheslav and Olsen, J{\'o}gvan Magnus Haugaard and
                Meloni, Simone and Ippoliti, Emiliano and Carloni, Paolo and
                Rothlisberger, Ursula},
      title = {{MiMiC Communication Library (v2.0.2)}},
      publisher = {GitLab},
      year = {2022},
      doi = {10.5281/zenodo.7497400},
      note = {See https://mimic-project.org/}
    }

    @misc{mimicpy:0.2.1,
      author = {Raghavan, Bharath and Schackert, Florian K.},
      title = {{MiMiCPy (v0.2.1)}},
      publisher = {GitLab},
      year = {2023},
      doi = {10.5281/zenodo.7746282},
      note = {See https://mimic-project.org/}
    }
