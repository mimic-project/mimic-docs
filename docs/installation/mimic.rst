MiMiC Framework
===============

The following installation guide provides quick and concise instructions to get
MiMiC running as quickly as possible. The MiMiC framework consists of two
components, namely the MiMiC library and the MiMiC communication library (MCL).
In the following, we provide the requirements for both components and the
instructions for building and installing each component.

Along with the MiMiC framework, we also provide the utility tool MiMiCPy, which
is a Python-based collection of tools that automates the setup of MiMiC-based
simulations. See :ref:`mimicpy` for installation instructions.

Throughout this guide, we primarily use Git to obtain the MiMiC framework
sources, but we also provide optional download links. In case you opt for the
latter option, just note that the directory names extracted from downloaded
files might differ.

Requirements
------------

A list of minimum requirements to build the MiMiC framework is given below.
Optional dependencies and supported external programs are also listed.

Minimum Requirements
~~~~~~~~~~~~~~~~~~~~

* CMake 3.12
* Fortran compiler (Fortran 2008 standard)
* C++ compiler (C++17 standard)
* MPI library (MPI 2.1 standard)

Optional Dependencies
~~~~~~~~~~~~~~~~~~~~~

* Shared-memory parallelism: Fortran compiler with OpenMP 3.0 support
* Building unit tests: `pFUnit <https://github.com/Goddard-Fortran-Ecosystem/pFUnit>`_ (MiMiC)
  and `GoogleTest and GoogleMock <https://github.com/google/googletest>`_ (MCL)

Supported External Programs
~~~~~~~~~~~~~~~~~~~~~~~~~~~

See :ref:`external-programs` for installation instructions.

* `CPMD 4.3 <https://www.cpmd.org>`_ (patch required)
* `GROMACS 2021 <https://manual.gromacs.org>`_ (patch required)
* `PLUMED 2 <https://www.plumed.org>`_ (through CPMD)


Build and Installation
----------------------

The two components of the MiMiC framework are configured, built, tested, and
installed using the CMake build system. The following instructions will guide
you through the process from download to installation.

Before we start, it is important to decide where MiMiC should be installed.
In this guide, we will use ``.local`` in user's home directory, but feel free
to choose whichever location you prefer::

  INSTALL_DIR=${HOME}/.local

In the following, we will refer to this variable to set ``CMAKE_INSTALL_PREFIX``
which determines the root of install directory. This means that after invoking
``make install``, everything will be installed in this directory using standard
GNU locations, i.e., ``lib`` (or ``lib64``), ``include``, ``share``, etc.

Moreover, to be consistent with the compilers used for the different programs
you are going to build, you can set the environment variables ``FC``, ``CC``,
and ``CXX`` to your chosen Fortran, C, and C++ compilers, respectively. These
are then detected by CMake during the build configuration process.

Finally, we recommend that you download the sources in a separate directory.
Here we will create and use a directory in the user's home directory, but again
feel free to use whichever location you prefer::

  SOURCE_DIR=${HOME}/mimic_sources
  mkdir -p ${SOURCE_DIR}

.. admonition:: Building with tests

    We strongly recommend to build with tests if you are planning to use the
    builds for production simulations. Note that some tests require either
    ``mpirun`` or ``mpiexec`` which may not be available on all HPC clusters
    that may instead use, e.g., ``srun``. If neither ``mpirun`` nor ``mpiexec``
    is available, then those tests may fail, but that does not necessarily
    mean that the build will not work. If you are in doubt consult the
    :ref:`Help section <help>`.

.. warning::

    Instructions in this guide are written for releases of MiMiC and MCL. Note
    that the development branch may be unstable or contain breaking changes
    that are not compatible with released versions of supported programs.
    Therefore we do not recommend that you use it unless you really know what
    you are doing.

MiMiC Communication Library
~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is necessary to build and install MCL first, as the MiMiC library depends on
it. Start by changing directory to the common source directory::

  cd ${SOURCE_DIR}

Then start by downloading and unpacking the release version of MCL::

  wget https://gitlab.com/mimic-project/mcl/-/archive/2.0.2/mcl-2.0.2.tar.gz
  tar -zxvf mcl-2.0.2.tar.gz

Alternatively, you can use git to clone it directly from the MCL repository.
Next, enter the MCL root directory, create a build directory and enter it::

  cd mcl-2.0.2
  mkdir build
  cd build

From here, you configure the build using CMake, and then build and install::

  cmake -DBUILD_SHARED_LIBS=YES -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} ..
  make
  make install

Additionally, you might also want to build and run tests. To do so, you need to
add ``-DBUILD_TESTS=YES`` in the configure step shown above and run
``make test`` before ``make install``. The prerequisite for this is the
GoogleTest testing and mocking framework. If you do not have it, you can build
it in a few steps. However, this has to be done before you configure MCL.

.. admonition:: Installing GoogleTest

    If you already have a functioning build of GoogleTest just set these
    environment variables::

      export GTEST_ROOT=<path to your GTest build>
      export GMOCK_ROOT=<path to your GMock build>

    Otherwise, you can clone the GoogleTest repository, build, and install by
    following these steps::

      cd ${SOURCE_DIR}
      wget -O googletest-1.12.1.tar.gz https://github.com/google/googletest/archive/refs/tags/release-1.12.1.tar.gz
      cd googletest-release-1.12.1
      mkdir build
      cd build
      cmake -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} ..
      make
      make install

.. admonition:: Failing tests

    Note that tests ``mpitransport`` and ``mpmdtransport`` require either
    ``mpirun`` or ``mpiexec`` which may not be available on all HPC clusters
    that may instead use, e.g., ``srun``. If neither ``mpirun`` nor ``mpiexec``
    is available, then those tests will fail, but that does not necessarily
    mean that the MCL build will not work. Moreover, not all MPI
    implementations support both modes of communication offered by MCL. In our
    experience, most newer MPI implementations support MPMD, which is tested by
    ``mpmdtransport``, while the server-client mechanism, which is tested by
    ``mpitransport``, is less common. There may also be others factors that
    result in those tests failing even if the MCL build is fully functional.
    If you are in doubt consult the :ref:`Help section <help>`.

MiMiC Library
~~~~~~~~~~~~~

Once we have installed the communication library, we can proceed to the
compilation of MiMiC. Move to the source directory::

  cd ${SOURCE_DIR}

And then you download and unpack the release version of MiMiC::

  wget https://gitlab.com/mimic-project/mimic/-/archive/0.2.0/mimic-0.2.0.tar.gz
  tar -zxvf mimic-0.2.0.tar.gz

Alternatively, you can use git to clone the repository. Enter the MiMiC root
directory and from there create a build directory and enter it::

  cd mimic-0.2.0
  mkdir build
  cd build

Then you configure the build by running CMake and then build and finally
install::

  cmake -DENABLE_OPENMP=YES -DBUILD_SHARED_LIBS=YES -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} ..
  make
  make install

As with MCL, we recommend that you also build and run the tests suite. This is
done in the same manner by adding a CMake flag ``-DBUILD_TESTS=YES`` when
configuring and calling ``make test`` before the installing. The only
difference is that MiMiC uses pFUnit testing framework, which you can install
following the instructions below.

.. admonition:: Installing pFUnit

    First clone pFUnit repository to the source directory::

      cd ${SOURCE_DIR}
      wget -O pFUnit-4.6.1.tar.gz https://github.com/Goddard-Fortran-Ecosystem/pFUnit/archive/refs/tags/v4.6.1.tar.gz
      tar -zxvf pFUnit-4.6.1.tar.gz

    Enter the directory, create and enter ``build`` directory, configure,
    build, and install::

      cd pFUnit-4.6.1
      mkdir build
      cd build
      cmake -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} ..
      make tests
      make install

    If you encounter an error related to git submodules, try using a more
    recent version of git.

.. admonition:: Failing tests

    Note that tests ``test_main`` and ``test_data_collect`` require either
    ``mpirun`` or ``mpiexec`` which may not be available on all HPC clusters.
    If neither is available, then those tests will fail, but that does not
    necessarily mean that the MiMiC build will not work. Both tests also rely
    on the MPMD communication mechanism and will fail if it is not supported.
    There may also be others factors that result in those tests failing even if
    the MiMiC build is fully functional. If you are in doubt consult the
    :ref:`Help section <help>`.
