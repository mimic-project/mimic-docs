.. _mimicpy:

MiMiCPy
=======

MiMiCPy is the companion utility package for MiMiC, with a collection of tools for
preparing and debugging MiMiC input files. It is written in Python and can be
installed from the Python Package Index (PyPI) using the package installer for Python
(pip). MiMiCPy can be installed directly in the current Python installation or within
a virtual environment. The process is described in detail below.

Requirements
------------

The minimum requirements for running MiMiCPy are given below. MiMiCPy also
provides plugins for the molecular visualization packages VMD and PyMOL.
The plugins can be installed anytime after the installation of MiMiCPy.

Minimum Requirements
~~~~~~~~~~~~~~~~~~~~

* Python 3.5+
* Pandas 0.24+
* NumPy 1.12+

Optional Dependencies
~~~~~~~~~~~~~~~~~~~~~

* VMD
* PyMOL

.. _pip mimicpy:

Basic Installation
------------------

To install the latest stable version of MiMiCPy from the PyPI, run the following command:

.. code-block:: bash

    $ python -m pip install mimicpy

This will also install the required dependencies if they are not already available.
Recent versions of `pip` will install packages by default in a local user directory
when root access is not available. The path depends on the system, but the base
directory defaults to :code:`~/.local` on Linux. Scripts are then placed in
:code:`~/.local/bin` which is often already added to the :code:`PATH` environment variable.
In case it is not, or you do not know the base directory, run this command to add it
to the :code:`PATH` environment variable:

.. code-block:: bash

	PATH=$(python -c 'import site; print(site.USER_BASE + "/bin")'):${PATH}

To check that MiMiCPy has been correctly installed, you can ask MiMiCPy to print the
`help` via the command:

.. code-block:: bash

    $ mimicpy --help

After successfully installing MiMiCPy, you can optionally install the PyMOL/VMD plugins
by running the plugin installer bundled with MiMiCPy:

.. code-block:: bash

	$ mimicpy_plugin_installer -pymoldir /path/to/plugin/ -vmddir /path/to/plugin/

The path to the plugin is usually either the path to the PyMOL/VMD installation, or the user home directory.
This command will create (or append to, if it already exists) a :code:`.pymolrc.py` and/or
a :code:`.vmdrc` (:code:`vmd.rc` on Windows) file in the given paths (in this case
:code:`/path/to/plugin/`). If these files are in the current directory, the user's home
directory, or the installation directory of the visualization package, they will be
read and loaded by PyMOL and VMD on startup. This makes the PrepQM command available to them.

Installing MiMiCPy from the PyPI is the recommended option. However, if you would like
to install the most recent version of MiMiCPy under development, clone the source using
git (or download it directly):

.. code-block:: bash

    $ python -m pip install git+https://gitlab.com/mimic-project/mimicpy.git

Python Virtual Environments
---------------------------

As mentioned in the previous section, :code:`pip` will often add install packages locally.
In these cases, the MiMiCPy executables may not be available without adding them to `PATH`.
This tedious process can be circumvented by creating virtual environments, which is the
recommended procedure for installing Python packages. Python has built-in support for
virtual environments that can be created and activated as follows:

.. code-block:: bash

    $ python -m venv mimicpy_venv
    $ . mimicpy_venv/bin/activate

This will create and activate a virtual environment that will be located in
the folder :code:`mimicpy_venv`, which is created in the directory from
where you run the commands. You can choose, in principle, any name for the
environment but avoid using names that clash with Python package names. The
virtual environment can be deactivated by running the following command:

.. code-block:: bash

    $ deactivate

To install MiMiCPy, run :code:`pip` from within the virtual environment (i.e., after activating it):

.. code-block:: bash

    $ python -m pip install mimicpy

And the VMD/PyMOL plugins:

.. code-block:: bash

	$ mimicpy_plugin_installer -pymoldir /path/to/plugin/ -vmddir /path/to/plugin/

To use MiMiCPy within VMD/PyMOL, activate the virtual environment before starting up VMD/PyMOL.

Conda Environments
------------------

Conda is package and environment management system that allows a user to create,
export, list, remove, and update environments that have different versions of Python
and/or Python packages installed in them. If you are not familiar with conda, check the
`conda installation instructions <https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html>`_
for details about the installation and the
`conda environments documentation <https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html>`_
for more information about conda environments in general.

If you have conda installed in your system, you can set up a conda environment for
MiMiCPy with the following commands (in this example, Python 3.7 is used):

.. code-block:: bash

    $ conda create --name mimicpy python=3.7 ipython
    $ conda activate mimicpy

This will create and activate a conda environment. The conda environment can be
deactivated by running the command:

.. code-block:: bash

    $ conda deactivate

To install MiMiCPy in your conda environment, you can use pip as explained above.
To this aim, you first have to install pip in your conda environment by running:

.. code-block:: bash

    $ conda activate mimicpy
    $ conda install pip

and then proceed as explained :ref:`previously<pip mimicpy>` if you want to install
MiMiCPy without VMD and/or PyMOL support.

Instead, if you want to install MiMiCPy with VMD and/or PyMOL support, you first need to
install VMD and/or PyMOL in the conda environment. This can be easily achieved with
the following commands:

.. code-block:: bash

    $ conda install vmd
    $ conda install -c schrodinger pymol

When installing MiMiCPy with VMD or PyMOL support, make sure to pass the path
corresponding to the VMD and/or PyMOL installation directory in the conda environment:

.. code-block:: bash

	$ pip install mimicpy
    $ mimicpy_plugin_installer -pymoldir /path/to/pymol/ -vmddir /path/to/vmd/
