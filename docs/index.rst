Welcome to MiMiC
================

MiMiC is a framework for multiscale modeling in computational chemistry. The
aim is to enable flexible and efficient implementations of multiscale
simulation methods with support for multiple subsystems through coupling
external programs.

The MiMiC framework consists of the MiMiC library and MCL (MiMiC communication
library). Additionally, the MiMiCPy package can be used to assist in input
preparation. MiMiC-based QM/MM runs efficiently in parallel using a combination
of MPI and OpenMP parallelization. It is freely available under the LGPLv3+
license.

.. figure:: images/mimic.png
   :width: 80 %
   :align: center
   :figwidth: 100 %

   **MiMiC supports coupling CPMD and GROMACS for extremely scalable QM/MM simulations**

**Getting started:**

#. Install :doc:`MiMiC framework</installation/mimic>`
#. Patch and install :doc:`CPMD and GROMACS </installation/externals>`
#. Install :doc:`MiMiCPy </installation/mimicpy>`
#. Check `known issues`_ in MiMiC
#. Follow the :doc:`introductory tutorial </tutorials/acetone_tutorial>`
#. Get :doc:`help and support </help_support>`
#. Join our `discussion group <https://groups.google.com/g/mimic-project>`_ to receive news and updates

.. _known issues: https://mimic-project.org/en/latest/help_support.html#known-issues

**Current releases:**

* MiMiC v0.2.0
* MCL v2.0.2
* MiMiCPy v0.2.1

.. figure:: images/homepage/code.png
   :width: 60 %
   :align: center
   :target: https://gitlab.com/mimic-project/
   :figwidth: 100 %

.. toctree::
   :hidden:
   :caption: Introduction

   about.rst
   contributors.rst
   features.rst
   citation.rst

.. toctree::
   :hidden:
   :caption: Installation

   installation/mimic.rst
   installation/externals.rst
   installation/mimicpy.rst

.. toctree::
   :hidden:
   :caption: Tutorials

   tutorials/acetone_tutorial.md
   tutorials/mimicpy_handbook.md

.. toctree::
   :hidden:
   :caption: Help and Support

   help_support.rst

.. toctree::
   :hidden:
   :caption: MiMiCPy

   mimicpy/overview.rst
   mimicpy/prepqm.rst
   mimicpy/other_commands.rst

.. toctree::
   :hidden:
   :caption: References

   references.rst
