Contributors
============

The MiMiC framework is developed mainly by the groups of J. M. H. Olsen,
P. Carloni, and U. Röthlisberger. The initial idea for MiMiC came about in a
meeting between J. M. H. Olsen, S. Meloni, and U. Röthlisberger at EPFL in
early 2015. The MiMiC concept was further developed in 2015 by J. M. H. Olsen
with contributions from S. Meloni and U. Röthlisberger.

The plans for MiMiC were presented to T. Laino and A. Curioni from
IBM Research Europe and V. Bolnykh, E. Ippoliti, and P. Carloni from
Forschungzentrum Jülich in a meeting hosted by IBM Research Europe in Zürich
on November 25th, 2015. Shortly after, E. Ippoliti and V. Bolnykh visited
EPFL, where the work on the MiMiC framework started. During the visit, the
implementation was designed by J. M. H. Olsen, V. Bolnykh, and S. Meloni, with
contributions from E. Ippoliti and U. Röthlisberger. The work on the MiMiC
framework continued as a collaboration from that time.

J. M. H. Olsen and V. Bolnykh wrote the MiMiC library, and V. Bolnykh wrote
the MiMiC communication library (MCL). J. M. H. Olsen and V. Bolnykh wrote the
CPMD interface, and V. Bolnykh wrote the GROMACS interface.

The following is a list of the main authors and other contributors including
their affiliation(s) at the time of their contribution.

Main authors:

- Jógvan Magnus Haugaard Olsen

  - Technical University of Denmark
  - UiT The Arctic University of Norway
  - École Polytechnique Fédérale de Lausanne

- Viacheslav Bolnykh

  - Forschungzentrum Jülich
  - RWTH Aachen University
  - The Cyprus Institute

Other contributors:

- Simone Meloni

  - University of Ferrara
  - École Polytechnique Fédérale de Lausanne

- Ursula Röthlisberger

  - École Polytechnique Fédérale de Lausanne

- Emiliano Ippoliti

  - Forschungzentrum Jülich

- Paolo Carloni

  - Forschungzentrum Jülich
  - RWTH Aachen University

- Martin P. Bircher

  - École Polytechnique Fédérale de Lausanne

- Till Kirsch

  - Johannes Gutenberg-Universität Mainz

- Jürgen Gauss

  - Johannes Gutenberg-Universität Mainz

- Michele Cascella

  - University of Oslo

- Bharath Raghavan

  - Forschungzentrum Jülich
  - RWTH Aachen University

- Florian Schackert

  - Forschungzentrum Jülich
  - RWTH Aachen University

- Nitin Malapally

  - Forschungzentrum Jülich

- Davide Mandelli

  - Forschungzentrum Jülich

- Andrea Levy

  - École Polytechnique Fédérale de Lausanne

- Sophia Johnson

  - École Polytechnique Fédérale de Lausanne

- Andrej Antalik

  - École Polytechnique Fédérale de Lausanne

- François Mouvet

  - École Polytechnique Fédérale de Lausanne

- Sonata Kvedaravičiūtė

  - Technical University of Denmark

- David Carrasco de Busturia

  - Technical University of Denmark
